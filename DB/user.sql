/*
Navicat MySQL Data Transfer

Source Server         : 123.56.220.149
Source Server Version : 50717
Source Host           : 123.56.220.149:3306
Source Database       : cloud

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2017-03-14 17:25:21
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `balance` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '刘大', 'liuda', '34', '10.00');
INSERT INTO `user` VALUES ('2', '关二', 'guaner', '12', '9.00');
INSERT INTO `user` VALUES ('3', '张三', 'zhangsan', '35', '8.00');
INSERT INTO `user` VALUES ('4', '赵四', 'zhaosi', '34', '7.00');
INSERT INTO `user` VALUES ('5', '黄五', 'huangwu', '87', '6.00');
INSERT INTO `user` VALUES ('6', '马六', 'maliu', '45', '5.00');
INSERT INTO `user` VALUES ('7', '魏七', 'weiqi', '56', '4.00');
INSERT INTO `user` VALUES ('8', '姜八', 'jiangba', '23', '3.00');
INSERT INTO `user` VALUES ('9', '严九', 'yanjiu', '43', '2.00');
INSERT INTO `user` VALUES ('10', '王十', 'wangshi', '21', '1.00');
