#springcloud 学习 
--pom配置没有做集中管理

1、 服务注册与发现

    cloud/cloud-simple-provider-user
    1)添加mybatis支持（单数据库链接）
    2)添加服务注册到cloud-eureka上
    
    
    cloud/cloud-simple-consumer-movie
    1)添加简单调用user服务
    
    
    cloud-eureka
    1）添加cloud发现服务的eureka
    
    
    cloud-provider
    1)添加服务注册到cloud-eureka上
    2)--server.port=8080  --server.port=8081  指定两个端口，启动，测试默认ribbon负载均衡规则（平均分配）
    
    
    cloud-ribbon
    1）调用cloud-eureka注册中心
  
        

