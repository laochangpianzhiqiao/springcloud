package com.cloud.controller;

import com.cloud.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author wangding
 * @version V1.0
 * @Description: todo
 * @date 2017/3/14
 */

@RestController
public class UserController {
    @Autowired
    RestTemplate restTemplate;

    @GetMapping("/queryUserById/{id}")
    public User queryUserById(@PathVariable Long id) {
        ResponseEntity<User> responseEntity = this.restTemplate.getForEntity("http://127.0.0.1:9090/queryUserById/" + id, User.class);
        return responseEntity.getBody();
    }

}
