package com.cloud.controller;

import com.cloud.feign.UserFeignClient;
import com.cloud.feign.UserFeignClientUrl;
import com.cloud.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangding
 * @version V1.0
 * @Description: todo
 * @date 2017/3/14
 */

@RestController
public class UserController {

    @Autowired
    UserFeignClient userFeignClient;
    @Autowired
    UserFeignClientUrl userFeignClientUrl;




    @GetMapping("/queryUserById/{id}")
    public User queryUserById(@PathVariable Long id) {
        return userFeignClient.queryUserById(id);
    }

    @GetMapping("/{serviceName}")
    public String findEurekaInfoByServiceName(@PathVariable("serviceName") String serviceName) {
        return userFeignClientUrl.findEurekaInfoByServiceName(serviceName);
    }


//    @GetMapping("/queryUser/{id}")
//    @ResponseBody
//    public User queryUser(@PathVariable Long id) {
//        return userFeignClient.queryUser(id);
//    }


}
