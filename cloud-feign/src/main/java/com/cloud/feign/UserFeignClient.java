package com.cloud.feign;

import com.cloud.model.User;
import com.conf.MyConfiguration;
import feign.Param;
import feign.RequestLine;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * @author 13787
 * @version V1.0
 * @Description: todo
 * @date 2017/5/8
 */
@FeignClient(name="cloud-provider",configuration = MyConfiguration.class)
public interface UserFeignClient {

//    @RequestMapping(value = "/queryUserById/{id}", method = RequestMethod.GET)
//    public User queryUserById(@PathVariable("id") Long id);

//    @RequestMapping(value ="/queryUser", method = RequestMethod.POST)
//    public User queryUser(@RequestParam("id") Long id);




    @RequestLine("GET /queryUserById/{id}")
    public User queryUserById(@Param("id") Long id);


}
