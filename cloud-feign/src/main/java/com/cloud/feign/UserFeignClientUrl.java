package com.cloud.feign;

import com.conf.MyConfigurationUserAndPwd;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author 13787
 * @version V1.0
 * @Description: todo
 * @date 2017/5/8
 */
@FeignClient(name = "cloud-test", url = "http://127.0.0.1:9090/",configuration = MyConfigurationUserAndPwd.class)
public interface UserFeignClientUrl {

    @RequestMapping(value = "/eureka/apps/{serviceName}",method = RequestMethod.GET)
    public String findEurekaInfoByServiceName(@PathVariable("serviceName") String serviceName);


}
