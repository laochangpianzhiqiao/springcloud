package com.conf;

import feign.Contract;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 13787
 * @version V1.0
 * @Description: todo
 * @date 2017/5/20
 */
@Configuration
public class MyConfiguration {
    @Bean
    public Contract feignContract() {
        return new  feign.Contract.Default();
    }

}
