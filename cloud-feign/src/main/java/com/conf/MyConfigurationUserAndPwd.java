package com.conf;

import feign.auth.BasicAuthRequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 13787
 * @version V1.0
 * @Description: todo
 * @date 2017/5/20
 */
@Configuration
public class MyConfigurationUserAndPwd {
    @Bean
    public BasicAuthRequestInterceptor basicAuthRequestInterceptor() {
        return new  BasicAuthRequestInterceptor("wangding","wangding");
    }

}
