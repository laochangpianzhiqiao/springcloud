package com.cloud.controller;

import com.cloud.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author wangding
 * @version V1.0
 * @Description: todo
 * @date 2017/3/14
 */

@RestController
public class UserController {
    @Autowired
    RestTemplate restTemplate;
    @Autowired
    private LoadBalancerClient loadBalancerClient;

    @GetMapping("/queryUserById/{id}")
    @ResponseBody
    public User queryUserById(@PathVariable Long id) {
        //虚拟ip cloud-provider
        ResponseEntity<User> responseEntity = this.restTemplate.getForEntity("http://cloud-provider/queryUserById/" + id, User.class);
        User user = responseEntity.getBody();
        return user;
    }

    @GetMapping("/queryUser/{id}")
    @ResponseBody
    public User queryUser(@PathVariable Long id) {
        //虚拟ip cloud-provider
        ServiceInstance serviceInstance = this.loadBalancerClient.choose("cloud-provider");

        System.out.println(serviceInstance.getServiceId()+":"+serviceInstance.getHost()+":"+serviceInstance.getUri());

        ResponseEntity<User> forEntity = this.restTemplate.getForEntity("http://cloud-provider/queryUserById/" + id, User.class);
        return forEntity.getBody();
    }



}
