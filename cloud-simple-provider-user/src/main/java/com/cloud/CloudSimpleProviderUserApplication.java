package com.cloud;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@MapperScan(basePackages = "com.cloud.mapper")
public class CloudSimpleProviderUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(CloudSimpleProviderUserApplication.class, args);
	}
}
