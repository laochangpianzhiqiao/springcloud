package com.cloud.controller;

import com.cloud.model.User;
import com.cloud.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangding
 * @version V1.0
 * @Description: todo
 * @date 2017/3/14
 */

@RestController
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("/queryUserById/{id}")
    public User queryUserById(@PathVariable Long id) {
        return userService.queryUserById(id);
    }

}
