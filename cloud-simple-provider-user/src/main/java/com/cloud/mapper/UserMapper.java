package com.cloud.mapper;

import com.cloud.database.BaseMapper;
import com.cloud.model.User;

/**
 * @author wangding
 * @version V1.0
 * @Description: todo
 * @date 2017/3/14
 */
public interface  UserMapper extends BaseMapper<User> {
}
