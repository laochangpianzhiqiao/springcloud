package com.cloud.model;

import com.cloud.database.BaseEntity;

import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * [STRATO MyBatis Generator]
 * Table: user
@mbggenerated do_not_delete_during_merge 2017-03-14 15:41:52
 */
@Table(name = "user")
public class User extends BaseEntity implements Serializable{

    private String username;
    private String name;
    private Integer age;
    private BigDecimal balance;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}