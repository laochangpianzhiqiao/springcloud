package com.cloud.service;

import com.cloud.mapper.UserMapper;
import com.cloud.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author wangding
 * @version V1.0
 * @Description: todo
 * @date 2017/3/14
 */
@Service("userService")
public class UserService {

    @Autowired
    UserMapper userMapper;

    public User queryUserById(Long id) {
        User user=userMapper.selectByPrimaryKey(id);
//        user=   userMapper.selectAll().get(0);
        return user;
    }

}
